package br.ufc.cloud.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.microsoft.windowsazure.services.blob.client.CloudBlob;
import com.microsoft.windowsazure.services.blob.client.CloudBlobClient;
import com.microsoft.windowsazure.services.blob.client.CloudBlobContainer;
import com.microsoft.windowsazure.services.blob.client.CloudBlockBlob;
import com.microsoft.windowsazure.services.blob.client.ListBlobItem;
import com.microsoft.windowsazure.services.core.storage.CloudStorageAccount;
import com.microsoft.windowsazure.services.core.storage.StorageException;

@ManagedBean
@RequestScoped
public class BlobStorageController {	
	public static final String storageConnectionString = "DefaultEndpointsProtocol=https;" 
						+ "AccountName=portalvhdsqncqmcgvtlnl6" + "AccountKey=IyrX/+54pL7N6/NVrvEvLP1BiRNXRmZy6zl7+OdHW60Mv/TMYqwOlZTlKBkhaC55/JpiW5/TkzfXm92gCIa1EQ==";

	public void uploadBlob(){
		try {
			CloudStorageAccount storageAccount = CloudStorageAccount.parse (storageConnectionString);
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			
			CloudBlobContainer container = blobClient.getContainerReference("images");
			container.createIfNotExist();
			
			CloudBlockBlob blob = container.getBlockBlobReference("images_2.jpg");
			File source = new File("C:\\imagensNuvem\\images_2.jpg");
			blob.upload(new FileInputStream(source), source.length());
			
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (StorageException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void downloadBlob(){
		try {
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			CloudBlobContainer container = blobClient.getContainerReference("images");
			
			for (ListBlobItem blobItem : container.listBlobs()) {
			    if (blobItem instanceof CloudBlob) {
			        CloudBlob blob = (CloudBlob) blobItem;
			        blob.download(new FileOutputStream(blob.getName()));
			        System.out.println("Blob Baixado com sucesso");
			    }
			}   
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (StorageException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void listBlob(){
		try {
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			CloudBlobContainer container = blobClient.getContainerReference("images");
			
			for (ListBlobItem blobItem : container.listBlobs()) {
			    System.out.println(blobItem.getUri());
			    System.out.println("Blobs LISTADOS com sucesso!");
			}
			
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}catch (StorageException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteBlob(){
		try {
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			CloudBlobContainer container = blobClient.getContainerReference("images");
			
			CloudBlockBlob blob = container.getBlockBlobReference("images_2.jpg");
			blob.delete();			
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (StorageException e) {
			e.printStackTrace();
		}
	}
}